# Nesecord Dark v0.2.1
**Nesecord Dark is a simple dark theme for Discord**  
  
![Screenshot1](images/nesecord-dark-screenshot1.png)

# Installation
- **Download and extract the project**  
- **Enter the extracted folder**  
- **Make sure you have enabled the dark mode in Discord**  
  
- ***BetterDiscord:***  
 - *Automatically:* Run the nesecord-bdinstaller.bat  
*Manually:* Put the `nesecord-dark.theme.css` file into the `%AppData%\BetterDiscord\themes` folder  
 - Restart Discord  
 - Go to the Discord settings, click on `Themes` and activate it  
  
- ***BeautifulDiscord:***  
 - Coming Soon
  
- ***Other custom CSS extension (e.g. "User CSS" for Chrome):***  
 - Open the `nesecord-dark.theme.css` and copy everything except the fist line into your custom CSS extension
  
- **Enjoy**

# Contact

[If you want you can join my **german** Discord server](https://discord.gg/A6YYg84) or just send me (Nesewebel#6185) a friend request.  
Twitter: [Nesewebel](https://twitter.com/Nesewebel)  
Email: [nesewebel@gmail.com](mailto:nesewebel@gmail.com)